object Form1: TForm1
  Left = 192
  Top = 125
  Width = 515
  Height = 515
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefaultSizeOnly
  PrintScale = poNone
  PixelsPerInch = 96
  TextHeight = 13
  object LbPedrasPreciosas: TLabel
    Left = 152
    Top = 32
    Width = 176
    Height = 24
    Caption = 'Pedras Preciosas'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Arial Rounded MT Bold'
    Font.Style = []
    ParentFont = False
  end
  object LbNome: TLabel
    Left = 48
    Top = 304
    Width = 75
    Height = 22
    Caption = 'Nome ->'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial Rounded MT Bold'
    Font.Style = []
    ParentFont = False
  end
  object LbRaridade: TLabel
    Left = 16
    Top = 336
    Width = 106
    Height = 22
    Caption = 'Raridade ->'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial Rounded MT Bold'
    Font.Style = []
    ParentFont = False
  end
  object LbValor: TLabel
    Left = 48
    Top = 368
    Width = 70
    Height = 22
    Caption = 'Valor ->'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial Rounded MT Bold'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 16
    Top = 400
    Width = 104
    Height = 22
    Caption = 'Atualizar ->'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial Rounded MT Bold'
    Font.Style = []
    ParentFont = False
  end
  object ListBoxPedras: TListBox
    Left = 128
    Top = 72
    Width = 225
    Height = 217
    ItemHeight = 13
    TabOrder = 0
  end
  object BtnInserir: TButton
    Left = 384
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Inserir'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Arial Rounded MT Bold'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    OnClick = BtnInserirClick
  end
  object BtnDeletar: TButton
    Left = 384
    Top = 128
    Width = 75
    Height = 25
    Caption = 'Deletar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Arial Rounded MT Bold'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnClick = BtnDeletarClick
  end
  object Btnatualizar: TButton
    Left = 384
    Top = 168
    Width = 75
    Height = 25
    Caption = 'Atualizar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Arial Rounded MT Bold'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = BtnatualizarClick
  end
  object BtnGravar: TButton
    Left = 384
    Top = 208
    Width = 75
    Height = 25
    Caption = 'Gravar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Arial Rounded MT Bold'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
  end
  object BtnSalvar: TButton
    Left = 384
    Top = 248
    Width = 75
    Height = 25
    Caption = 'Salvar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Arial Rounded MT Bold'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
  end
  object EdNome: TEdit
    Left = 128
    Top = 304
    Width = 225
    Height = 21
    TabOrder = 6
  end
  object EdRaridade: TEdit
    Left = 128
    Top = 336
    Width = 225
    Height = 21
    TabOrder = 7
  end
  object EdValor: TEdit
    Left = 128
    Top = 368
    Width = 225
    Height = 21
    TabOrder = 8
  end
  object EdAtualizar: TEdit
    Left = 128
    Top = 400
    Width = 225
    Height = 21
    TabOrder = 9
  end
end
